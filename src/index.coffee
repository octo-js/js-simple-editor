class SimpleEditor

  constructor : ( element ) ->

    @_wraper = SimpleEditor.isHTMLElement element
    @_editor = SimpleEditor.isHTMLElement @_wraper.querySelector '[ed-editor-area]'

    @init()

  loadActionButtons : ->
    actions = @_wraper.querySelectorAll '[ed-action]'
    for action in actions
      @addAction action

  init : ->
    # set editable
    @_editor.contentEditable = true

    # style
    height = @_editor.style.height
    height = '100px' if not height
    @_editor.style.minHeight = height
    @_editor.style.maxHeight = height

    @_editor.style.overflowY = 'auto'

    document.execCommand 'defaultParagraphSeparator', false, 'p'

    # load action buttons
    @loadActionButtons()

    @

  getHTML : -> @_editor.innerHTML

  @isHTMLElement : ( element ) ->
    if element instanceof HTMLElement
      return element

    else if /^#/.test( element )
      return SimpleEditor.isHTMLElement document.getElementById element.replace /^#/, ''
    else if /^\./.test( element )
      $elem = document.getElementsByClassName element.replace /^\./, ''
      return SimpleEditor.isHTMLElement $elem[0]

    # false
    throw new Error 'Element is not instance of HTMLElement'

  addAction : ( element ) ->
    throw new Error 'element is not instance of HTMLElement' if not ( element instanceof HTMLElement )

    # disable clickable
    element.style.touchCallout = 'none'
    element.style.userSelect   = 'none'

    element.onclick = ( ev ) ->
      ev.preventDefault()
      action = @getAttribute 'ed-action'
      switch action
        when 'h1', 'h2', 'h3', 'p'
          document.execCommand 'formatBlock', false, action
        when 'insertText'
          text = @getAttribute 'ed-text'
          document.execCommand action, false, text
        else
          document.execCommand action, false, null

window.SimpleEditor = SimpleEditor

